# LiqPay Integration Module

## Description
This module integrates the **LiqPay** payment system with your **Drupal** site.  
It allows you to customize payment settings and process data from the 
payment service.

## Module Features
- Flexible customization of payment parameters before sending them to LiqPay.
- Handling responses for successful and processed transactions through 
the LiqPay API.

## Installation
1. Place the module in the `modules/custom` directory of your site.
2. Enable the module via the Drupal admin panel or Drush:
   ```bash
   drush en liqpay
   ```
3. Configure the LiqPay API keys in the module's settings.

## Configuration
- Navigate to Admin Panel > Configuration > LiqPay.
- Enter your public and private API keys.
- Adjust the necessary parameters as needed.
